module.exports = {
  semi: false,
  singleQuote: true,
  arrowParens: 'avoid',
  trailingComma: 'all',
  bracketSpacing: false,
  jsxBracketSameLine: true,
};
