# THREE.js

- Site: <https://threejs.org/>

- Conceitos: <https://threejs.org/manual/>

## Projeto Base

- <http://3dmockuper.com/coffeemug.html>

- Código fonte da xícara: ./3dmockuper.com/cdn-0.3dmockuper.com/js/coffeeMugController.js

## Executando Projeto

- Acesse o fonte do projeto: ./src/paginas/xicara.html

- Abra esse arquivo no navegador, ou abrar no Visual Studio Code e execute via plugin, Live Server.

## Utilizando o Editor 3D

Acesse: <https://threejs.org/editor/> para editar via interface gráfica.

- Para salvar o projeto, clique em: File > Export Scene

_Obs: os arquivos da pasta ./src/modelagem podem ser importados no editor online._

### Exemplo de importação de objetos de outras aplicações

- 1 - Crie o gráfico 3d no Blender ou em outro software de modelagem 3D.
- 2 - Exporte em wavefront (.obj).
- 3 - Acesse o editor online do Threejs: <https://threejs.org/editor/>.
- 4 - Importe o arquivo .obj criado no passo 2.
- 5 - Selecione o objeto.
- 6 - Clique em File > Export Object.
- 7 - Com o arquivo baixado (.json), incluir no projeto pelo THREE.ObjectLoader.
